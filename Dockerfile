FROM adoptopenjdk/openjdk11:x86_64-ubuntu-jdk-11.0.19_7-slim
COPY . /app
WORKDIR /app
RUN ./mvnw clean install
EXPOSE 8080
ENTRYPOINT [ "java", "jar", "target/app.jar" ]